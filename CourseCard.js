import { useState} from 'react';

import {Container, Col, Row, Card, Button} from "react-bootstrap"
import propTypes from 'prop-types';

export default function CourseCard({courseProp}) {

	const {name, description, price} = courseProp

	const [count, setCount] = useState(0)
	const [seats, setSeats] = useState(10)

	function enroll() {

		if(seats > 0){
			setCount(count + 1);
			setSeats(seats - 1);
		}else {
			alert("no more seats")
		}

	}

	return( 
		
		<Container fluid className="mb-4">
			<Row className="justify-content-center" >
				<Col xs={10} md={8}>
					<Card className="cardHighlights p-3">
					  <Card.Body>
					    <Card.Title>{name}</Card.Title>
					    <Card.Subtitle>Description:</Card.Subtitle>
					    <Card.Text>{description}</Card.Text>

					     <Card.Subtitle> Price: </Card.Subtitle>
					     <Card.Text>{price}</Card.Text>
					     <Card.Text>Enrollees: {count}</Card.Text>
					    <Button variant="primary" onClick={enroll}> Enroll </Button>
					  </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}

CourseCard.propTypes = {
	courseProp: propTypes.shape({
		name: propTypes.string.isRequired,
		description: propTypes.string.isRequired,
		price: propTypes.number.isRequired
	})
}